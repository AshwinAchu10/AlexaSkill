module.change_code = 1;
'use strict';

var alexa = require('alexa-app');
var app = new alexa.app('alexa-skill');
var pushNoticatoin = require('./pushNotification');


app.launch(function (request, response) {
	response.say('Welcome to your custom skill').reprompt('Way to go. You got it to run.').shouldEndSession(false);
});


app.error = function (exception, request, response) {
	console.log(exception)
	console.log(request);
	console.log(response);
	response.say('Sorry an error occured ' + error.message);
};

app.intent('TellPatientDetailsIntent',
	{
		"slots": { 

			"name": "string",
			"drug":"string",
			"disease":"string"
	 }
		, "utterances": [

			"patient name is {Name} and his drug is {Drug} and his disease is {Disease}"
		]
	},
	function (request, response) {
		var name = request.slot('name');
		var drug = request.slot('drug');
		var disease = request.slot('disease');
		response.say("patient " + name + ", his disease is ," +disease+ ", for that he is using " +drug + ", Thank You");

		pushNoticatoin.pushNotification(name, drug, disease);

	});



module.exports = app;
